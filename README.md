# Laravel Base ADR Hexagonal

    Séparer explicitement User-Side, Business Logic et Server-Side
    Les dépendances vont vers la Business Logic
    On isole les frontières par des Ports et Adapters

L'objectif est ici de minimiser au maximum le lien fort entre notre logique métier 
et les différentes librairies et frameworks utilisés
Ce projet est une base pour lancer un projet Laravel sous docker.

Il tends a respecter 2 principes d'architectures : ADR et hexagonal

L’architecture hexagonale s’appuie sur trois principes et techniques :

    Séparer explicitement User-Side, Business Logic et Server-Side
    Les dépendances vont vers la Business Logic
    On isole les frontières par des Ports et Adapters

L'objectif est ici de minimiser au maximum le lien fort entre notre logique métier 
et les différentes librairies et frameworks utilisé

ADR (Action – domaine – répondeur) est un modèle d'architecture logicielle proposé par comme un raffinement de MVC qui est mieux adapté aux applications Web. 
Semblable à MVC, le modèle est divisé en trois parties :

    L' action prend des requêtes HTTP ( URL et leurs méthodes) et utilise cette entrée pour interagir avec le domaine , après quoi elle transmet la sortie du domaine à un et un seul répondeur . 
    Le domaine peut modifier l'état, interagir avec le stockage et / ou manipuler les données selon les besoins. Il contient la logique métier. 
    Le répondeur construit la réponse HTTP entière à partir de la sortie du domaine qui lui est donnée par l' action.

ADR ne doit pas être confondu avec un changement de nom de MVC ! 
Car même si certaines similitudes existent (Le modèle MVC est très similaire au domaine ADR) la différence réside dans le comportement: 

    Dans MVC, la vue peut envoyer des informations ou modifier le modèle , alors qu'en ADR, le domaine ne reçoit que les informations de l' action , pas du répondeur .
     
    Dans MVC Web-centric, la vue est simplement utilisée par le contrôleur pour générer le contenu d'une réponse, que le contrôleur pourrait ensuite manipuler avant de l'envoyer en sortie. 
    Dans ADR, le contrôle d'exécution passe au répondeur une fois que l' action a fini d'interagir avec le domaine , et donc le répondeur est entièrement responsable de la génération de toute la sortie. Le répondeur peut alors utiliser n'importe quel système de vue ou de modèle dont il a besoin. 
    
    Les contrôleurs MVC contiennent généralement plusieurs méthodes qui, lorsqu'elles sont combinées dans une seule classe, nécessitent une logique supplémentaire pour être gérées correctement, comme les hooks pré et post-action. 
    Cependant, chaque action ADR est représentée par des classes ou fermetures distinctes. En termes de comportement, l' action interagit avec le domaine de la même manière que le contrôleur MVC interagit avec le modèle , sauf que l' action n'interagit pas ensuite avec un système de vue ou de modèle, mais passe plutôt le contrôle au répondeur qui gère cela.
