<?php

declare(strict_types=1);

namespace Src\Responders\Home;

use Src\Responders\AbstractResponder;

final class WelcomeShowResponder extends AbstractResponder
{
    protected string $template = 'welcome';
}
