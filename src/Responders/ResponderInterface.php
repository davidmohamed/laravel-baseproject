<?php

declare(strict_types=1);

namespace Src\Responders;

use Src\Ports\RequestInterface;
use Src\Ports\ResponseInterface;

interface ResponderInterface
{
    public function handleRequest(RequestInterface $originalRequest): self;

    public function send(array $data = [], array $state = ['code' => 200, 'message' => 'success']): ResponseInterface;
}
