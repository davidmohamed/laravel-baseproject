<?php

declare(strict_types=1);

namespace Src\Responders\Monitoring;

use Src\Responders\AbstractResponder;

class StatusReadResponder extends AbstractResponder
{
    protected string $template = 'monitoring.response';
}
