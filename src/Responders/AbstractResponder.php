<?php

declare(strict_types=1);

namespace Src\Responders;

use Src\Adapters\OutputManager;
use Src\Ports\RequestInterface;
use Src\Ports\ResponseInterface;

abstract class AbstractResponder implements ResponderInterface
{
    protected RequestInterface $request;

    protected string $template;

    protected OutputManager $outputManager;

    public function __construct(OutputManager $outputManager)
    {
        $this->outputManager = $outputManager;
    }

    public function send(array $data = [], array $state = ['code' => 200, 'message' => 'success']): ResponseInterface
    {
        $state['template'] = $this->template;
        $outputFormater = $this->outputManager->getOutput($this->request->getRouteParameter('format') ?? 'html');
        $outputFormater->setOptions($state);

        return $outputFormater->render($data);
    }

    /**
     * @return AbstractResponder
     */
    public function handleRequest(RequestInterface $originalRequest): ResponderInterface
    {
        $this->request = $originalRequest;

        return $this;
    }
}
