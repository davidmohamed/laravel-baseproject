<?php

declare(strict_types=1);

namespace Src\Actions\Home;

use Src\Actions\AbstractAction;
use Src\Adapters\Request;
use Src\Ports\FrameworkInterface;
use Src\Ports\ResponseInterface;
use Src\Responders\Home\WelcomeShowResponder;

final class WelcomeShowAction extends AbstractAction
{
    public function __construct(WelcomeShowResponder $responder, Request $request, FrameworkInterface $framework)
    {
        parent::__construct($responder, $request, $framework);
    }

    public function __invoke(): ResponseInterface
    {
        return $this->getResponder()->send();
    }
}
