<?php

declare(strict_types=1);

namespace Src\Actions\Monitoring;

use Src\Actions\AbstractAction;
use Src\Adapters\Request;
use Src\Domains\Monitoring\Status;
use Src\Ports\FrameworkInterface;
use Src\Ports\ResponseInterface;
use Src\Responders\Monitoring\StatusReadResponder;

final class StatusReadAction extends AbstractAction
{
    public function __construct(StatusReadResponder $responder, Request $request, FrameworkInterface $framework)
    {
        parent::__construct($responder, $request, $framework);
    }

    public function __invoke(): ResponseInterface
    {
        $status = new Status($this->getFramework());

        return $this->getResponder()->send($status->getData(), $status->getState());
    }
}
