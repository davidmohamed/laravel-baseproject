<?php

declare(strict_types=1);

namespace Src\Actions;

use Src\Adapters\Request;
use Src\Ports\FrameworkInterface;
use Src\Ports\ResponseInterface;
use Src\Responders\ResponderInterface;

abstract class AbstractAction
{
    private ResponderInterface $responder;

    private FrameworkInterface $framework;

    public function __construct(ResponderInterface $responder, Request $request, FrameworkInterface $framework)
    {
        $this->framework = $framework;
        $this->responder = $responder;
        $responder->handleRequest($request);
    }

    abstract public function __invoke(): ResponseInterface;

    protected function getResponder(): ResponderInterface
    {
        return $this->responder;
    }

    protected function getFramework(): FrameworkInterface
    {
        return $this->framework;
    }
}
