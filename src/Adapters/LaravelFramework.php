<?php

declare(strict_types=1);

namespace Src\Adapters;

use Src\Ports\FrameworkInterface;

class LaravelFramework implements FrameworkInterface
{
    /**
     * @return mixed
     */
    public function getConfig(string $key)
    {
        return app('config')->get($key);
    }
}
