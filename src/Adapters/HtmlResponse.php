<?php

declare(strict_types=1);

namespace Src\Adapters;

use Src\Ports\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as FrameworkHtmlResponse;

class HtmlResponse extends FrameworkHtmlResponse implements ResponseInterface
{
}
