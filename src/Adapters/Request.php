<?php

declare(strict_types=1);

namespace Src\Adapters;

use Illuminate\Http\Request as LaravelRequest;
use Src\Ports\RequestInterface;

class Request implements RequestInterface
{
    private LaravelRequest $request;

    public function __construct(LaravelRequest $request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getRouteParameter(string $key)
    {
        return $this->request->route($key);
    }
}
