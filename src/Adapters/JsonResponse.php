<?php

declare(strict_types=1);

namespace Src\Adapters;

use Illuminate\Http\JsonResponse as FrameworkJsonResponse;
use Src\Ports\ResponseInterface;

class JsonResponse extends FrameworkJsonResponse implements ResponseInterface
{
}
