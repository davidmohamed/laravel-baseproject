<?php

declare(strict_types=1);

namespace Src\Adapters\Output;

use Src\Adapters\JsonResponse;
use Src\Ports\ResponseInterface;

class JsonOutput extends AbstractOutput
{
    public function render(array $data, int $code = 200, array $headers = []): ResponseInterface
    {
        return new JsonResponse($data, $code, $headers);
    }
}
