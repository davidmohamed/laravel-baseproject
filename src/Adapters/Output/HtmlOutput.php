<?php

declare(strict_types=1);

namespace Src\Adapters\Output;

use Illuminate\Support\Facades\View;
use Src\Adapters\HtmlResponse;
use Src\Ports\ResponseInterface;

class HtmlOutput extends AbstractOutput
{
    protected string $template;

    public function __construct()
    {
        $this->authorizedOptions[] = 'template';
    }

    public function render(array $data, int $code = 200, array $headers = []): ResponseInterface
    {
        $content = View::make($this->template, $data)->render();

        return new HtmlResponse($content, $code, $headers);
    }
}
