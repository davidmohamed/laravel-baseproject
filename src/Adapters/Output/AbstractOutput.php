<?php

declare(strict_types=1);

namespace Src\Adapters\Output;

use Src\Ports\OutputInterface;

abstract class AbstractOutput implements OutputInterface
{
    protected array $authorizedOptions = ['code', 'message'];

    public function setOptions(array $options): void
    {
        foreach ($options as $option => $value) {
            if (\in_array($option, $this->authorizedOptions, true)) {
                $this->$option = $value;
            }
        }
    }
}
