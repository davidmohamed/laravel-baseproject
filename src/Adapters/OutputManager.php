<?php

declare(strict_types=1);

namespace Src\Adapters;

use Src\Adapters\Output\HtmlOutput;
use Src\Adapters\Output\JsonOutput;
use Src\Ports\OutputInterface;
use Src\Ports\OutputManagerInterface;

class OutputManager implements OutputManagerInterface
{
    public function getOutput(string $extension = ''): OutputInterface
    {
        switch (strtolower(trim($extension, '.'))) {
            case 'json':
                $instance = new JsonOutput();
                break;

            case 'html':
            default:
                $instance = new HtmlOutput();
                break;
        }

        return $instance;
    }
}
