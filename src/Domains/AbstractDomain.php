<?php

declare(strict_types=1);

namespace Src\Domains;

class AbstractDomain implements DomainInterface
{
    protected int $code = 200;
    protected array $data;
    protected string $message = 'Success';

    public function getState(): array
    {
        return ['code' => $this->code, 'msg' => $this->message];
    }

    public function getData(): array
    {
        return $this->data;
    }
}
