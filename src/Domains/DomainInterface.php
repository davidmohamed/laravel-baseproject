<?php

declare(strict_types=1);

namespace Src\Domains;

interface DomainInterface
{
    public function getData(): array;
}
