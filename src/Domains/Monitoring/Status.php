<?php

declare(strict_types=1);

namespace Src\Domains\Monitoring;

use Src\Domains\AbstractDomain;
use Src\Ports\FrameworkInterface;

final class Status extends AbstractDomain
{
    public function __construct(FrameworkInterface $app)
    {
        $this->data = [
            'env' => $app->getConfig('app.env'),
            'version' => $app->getConfig('app.version'),
            'deploy' => sprintf('%s', date('Y-m-d H:i:s', getlastmod())),
        ];
    }
}
