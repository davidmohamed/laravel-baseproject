<?php

declare(strict_types=1);

namespace Src\Ports;

interface RequestInterface
{
    /**
     * @return mixed
     */
    public function getRouteParameter(string $key);
}
