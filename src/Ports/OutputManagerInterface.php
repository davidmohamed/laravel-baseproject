<?php

declare(strict_types=1);

namespace Src\Ports;

interface OutputManagerInterface
{
    public function getOutput(string $extension): OutputInterface;
}
