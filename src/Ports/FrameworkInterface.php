<?php

declare(strict_types=1);

namespace Src\Ports;

interface FrameworkInterface
{
    /**
     * @return mixed
     */
    public function getConfig(string $key);
}
