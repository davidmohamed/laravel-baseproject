<?php

declare(strict_types=1);

namespace Src\Ports;

interface OutputInterface
{
    public function setOptions(array $options): void;

    public function render(array $data): ResponseInterface;
}
