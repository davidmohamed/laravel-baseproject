##### Fonctionnalités métier

    1 Afficher une page d'accueil
    2 Afficher un utilisateur
    3 Afficher un compte
    4 Afficher la liste des comptes pour un utilisateur
    5 Afficher une opération
    6 Afficher une liste des opérations liées à un compte
    7 Afficher une catégorie
    8 Lister les opérations liées a une catégorie
    9 Afficher une liste des catégories disponibles pour un utilisateur
    
    Afficher un tableau de bord des différents comptes et opérations une fois connecté

    Ajouter un utilisateur
    Editer un utilisateur
    Supprimer un utilisateur
    
    Ajouter un compte
    Editer un compte
    Supprimer un compte
    
    Saisir une opération dans un compte
    Editer une opération
    Supprimer une opération
    
    Créer une catégorie
    Editer une catégorie
    Supprimer une catégorie
    Associer une opération à une catégorie
    
    Afficher une graphique de répartition des opérations et de leur montant par catégories
    
##### Fonctionnalités technique
    Enregistrer les entités en BDD
    Rechercher des opérations
    Authentification des utilisateurs
    Import d'opérations depuis fichier de banque
    Export de données personnelles (RGPD)
    Suppression/Anonymisation de données personnelles (RGPD)
