<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Src\Adapters\LaravelFramework;
use Src\Adapters\OutputManager;
use Src\Ports\FrameworkInterface;
use Src\Ports\OutputManagerInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OutputManagerInterface::class, function () {
            return new OutputManager();
        });
        $this->app->singleton(FrameworkInterface::class, function () {
            return new LaravelFramework();
        });
        $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
        $this->app->register(TelescopeServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
