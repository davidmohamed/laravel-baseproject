# variables
DOCKER_COMPOSE = USER_ID=`id -u` GROUP_ID=`id -g` docker-compose
EXEC_PHP = $(DOCKER_COMPOSE) exec -T app
EXEC_COMPOSER = $(DOCKER_COMPOSE) exec -T app composer
ARTISAN = $(EXEC_PHP) php artisan
COMPOSER = $(EXEC_COMPOSER)
NODE = $(DOCKER_COMPOSE) run --rm node
NPM = $(NODE) npm

# help/self-documented as default command
.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

###-------------------------#
###    Project Commands     #
###-------------------------#
auto-install: config build vendor npm generate-key ## initialize automatiquement project.

install: build vendor permissions ## initialize project.

rebuild: kill install ## Alias combining down, install

reset: rebuild start ## remove dependencies and reboot project

restart: stop start ## Alias for stop and start command

start: up ## start project.

stop: down ## stop project.

generate-key: ##generate app key
	$(EXEC_PHP) php artisan key:generate

.PHONY: auto-install generate-key install rebuild reset restart start stop

###-------------------------#
###     Docker Commands     #
###-------------------------#
build: ## Build all services
	$(DOCKER_COMPOSE) up -d --remove-orphans --build --force-recreate

build-db: ## Build mysql service
	$(DOCKER_COMPOSE) up -d --remove-orphans --build db

build-nginx: ## Build nginx service
	$(DOCKER_COMPOSE) up -d --remove-orphans --build nginx

build-node: ## Build node service
	$(DOCKER_COMPOSE) up -d --remove-orphans --build node

config:
	cp .env.dist .env || true
	cp docker-compose.yml.dist docker-compose.yml || true
	cp -f composer.json.dist composer.json
	cp -f phpunit.xml.dist phpunit.xml
	mkdir -p storage/framework/sessions ||true
	mkdir -p storage/framework/views ||true
	mkdir -p storage/framework/cache ||true
	sed -i "s/<applicationName>/$(shell basename $(CURDIR))/g" .env || true
	sed -i "s/<applicationName>/$(shell basename $(CURDIR))/g" docker-compose.yml || true
	sed -i "s/<currentUser>/$(shell echo `id -u`)/g" docker-compose.yml || true
	sed -i "s/<currentGroup>/$(shell echo `id -g`)/g" docker-compose.yml || true
	sed -i "s/<applicationName>/$(shell basename $(CURDIR))/g" composer.json || true
	sed -i "s/<author>/$(shell echo $(USER))/g" composer.json || true

down: ## Stop current docker-compose services and orphans services
	$(DOCKER_COMPOSE) stop

install-light:
	$(DOCKER_COMPOSE) up -d --remove-orphans --build db
	$(DOCKER_COMPOSE) up -d --remove-orphans --force-recreate

kill: ## Fully destroy current docker-compose services by stopping them and removing local images and docker managed volumes.
	$(DOCKER_COMPOSE) down --remove-orphans --volumes --rmi local

permissions: ## fix permissions if needed
	$(EXEC_PHP) chown -R fury:www-data /var/www
	$(EXEC_PHP) chmod -R 0777 /var/www

up: ## up docker-compose daemon
	$(DOCKER_COMPOSE) up -d --remove-orphans --force-recreate

.PHONY: build build-db build-nginx config down install-light kill permissions up

###-------------------------#
###      Tools              #
###-------------------------#

migrations: ## Run migrations
	$(ARTISAN) migrate

migrations-rollback: ## Run migrations rollback
	$(ARTISAN) migrate:rollback

telescope: ## Configure telescope
	$(ARTISAN) telescope:install
	$(ARTISAN) migrate
	$(ARTISAN) telescope:publish

bash: ## launch console bash on php container
	$(DOCKER_COMPOSE) exec app bash

vendor: ## composer install
	$(COMPOSER) install

vendor-clean: clean ## Remove all the cache, vendor and node_modules
	$(EXEC_PHP) rm -rf vendor

vendor-update: ## composer update
	$(COMPOSER) update

.PHONY: vendor-clean bash permissions
###-------------------------#
###      NPM Commands      #
###-------------------------#
node_modules: package.json ## Install npm dependencies
	$(NPM) install --silent

npm: node_modules ## Init of public/build directory
	$(NPM) run dev

npm-watch: node_modules ## Init of public/build directory
	$(NPM) run watch

.PHONY: node_modules npm
###-------------------------#
###      XDEBUG Commands      #
###-------------------------#

xdebug-on: ## enable xdebug
	$(EXEC_PHP) phpenmod -s fpm xdebug
	$(EXEC_PHP) phpenmod -s cli xdebug
	$(EXEC_PHP) /etc/init.d/php-fpm reload

xdebug-off: ## disable xdebug
	$(EXEC_PHP) phpdismod -s fpm xdebug
	$(EXEC_PHP) phpdismod -s cli xdebug
	$(EXEC_PHP) /etc/init.d/php-fpm reload
.PHONY: xdebug-on xdebug-off

###-------------------------#
###           QA            #
###-------------------------#
test: phpunit phpcs security phpmd phpstan

phpunit: ## Run only phpunit tests
	$(EXEC_PHP) cp phpunit.xml.dist phpunit.xml || true
	$(EXEC_PHP) vendor/bin/phpunit --configuration phpunit.xml --no-coverage --colors=never

phpunit-cc: ## Run only phpunit tests with code coverage (Warning : this command enable xdebug)
	$(EXEC_PHP) phpenmod -s fpm xdebug
	$(EXEC_PHP) phpenmod -s cli xdebug
	$(EXEC_PHP) /etc/init.d/php-fpm reload
	$(EXEC_PHP) vendor/bin/phpunit --configuration phpunit.xml --coverage-text --colors=never
	$(EXEC_PHP) chown -Rf www-data:www-data build/

behat: ## Run only behat tests
	$(EXEC_PHP) rm fail_*.png || true
	$(EXEC_PHP) vendor/bin/behat -f pretty $(ARGS)

behat-cc: ## Run only behat tests with codecoverage
	$(EXEC_PHP) rm fail_*.png || true
	$(EXEC_PHP) vendor/bin/behat -f html -f pretty

phpcs: ## Run only php-cs-fixer without fix
	$(EXEC_PHP) vendor/bin/php-cs-fixer fix --dry-run --no-interaction

phpcsfix: ## Run only php-cs-fixer
	$(EXEC_PHP) vendor/bin/php-cs-fixer fix --no-interaction

security:		## Run SecurityChecker
	$(EXEC_PHP) vendor/bin/security-checker security:check

phpmd:			## Run PHPMD
	$(EXEC_PHP) vendor/bin/phpmd src text .phpmd.xml

phpstan:		## Run PHPStan
	$(EXEC_PHP) vendor/bin/phpstan  --memory-limit=500 analyse src tests

.PHONY: test phpcs phpcsfix phpunit-coverage security db-init phpmd behat-cc behat
